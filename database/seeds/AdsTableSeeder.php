<?php

use Illuminate\Database\Seeder;
use App\Ad;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 1,
          'title' => 'Ad 1',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 1,
          'title' => 'Ad 2',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 1,
          'title' => 'Ad 3',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 1,
          'title' => 'Ad 4',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 2,
          'title' => 'Ad 5',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 2,
          'title' => 'Ad 6',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 2,
          'title' => 'Ad 7',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 2,
          'title' => 'Ad 8',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 3,
          'title' => 'Ad 9',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 3,
          'title' => 'Ad 10',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 3,
          'title' => 'Ad 11',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 3,
          'title' => 'Ad 12',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 4,
          'title' => 'Ad 13',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 4,
          'title' => 'Ad 14',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 4,
          'title' => 'Ad 15',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 4,
          'title' => 'Ad 16',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 5,
          'title' => 'Ad 17',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 5,
          'title' => 'Ad 18',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 5,
          'title' => 'Ad 19',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 5,
          'title' => 'Ad 20',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 6,
          'title' => 'Ad 21',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 6,
          'title' => 'Ad 22',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 6,
          'title' => 'Ad 23',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 6,
          'title' => 'Ad 24',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 7,
          'title' => 'Ad 25',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 7,
          'title' => 'Ad 26',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 7,
          'title' => 'Ad 27',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 7,
          'title' => 'Ad 28',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 8,
          'title' => 'Ad 29',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 3,
          'subcategory_id' => 8,
          'title' => 'Ad 30',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 8,
          'title' => 'Ad 31',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 8,
          'title' => 'Ad 32',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 1,
          'subcategory_id' => 9,
          'title' => 'Ad 33',
          'description' => 'A super awesome description.',
          'price' => 1000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 9,
          'title' => 'Ad 34',
          'description' => 'A super awesome description.',
          'price' => 2000 * 100,
          'is_new' => 0
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 9,
          'title' => 'Ad 35',
          'description' => 'A super awesome description.',
          'price' => 3000 * 100,
          'is_new' => 1
        ]);

        Ad::create([
          'user_id' => 2,
          'subcategory_id' => 9,
          'title' => 'Ad 36',
          'description' => 'A super awesome description.',
          'price' => 4000 * 100,
          'is_new' => 0
        ]);
    }
}
