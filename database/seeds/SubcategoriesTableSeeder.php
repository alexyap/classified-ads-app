<?php

use Illuminate\Database\Seeder;
use App\Subcategory;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subcategory::create([
          'category_id' => 1,
          'name' => 'Subcategory 1'
        ]);

        Subcategory::create([
          'category_id' => 1,
          'name' => 'Subcategory 2'
        ]);

        Subcategory::create([
          'category_id' => 1,
          'name' => 'Subcategory 3'
        ]);

        Subcategory::create([
          'category_id' => 2,
          'name' => 'Subcategory 4'
        ]);

        Subcategory::create([
          'category_id' => 2,
          'name' => 'Subcategory 5'
        ]);

        Subcategory::create([
          'category_id' => 2,
          'name' => 'Subcategory 6'
        ]);

        Subcategory::create([
          'category_id' => 3,
          'name' => 'Subcategory 7'
        ]);

        Subcategory::create([
          'category_id' => 3,
          'name' => 'Subcategory 8'
        ]);

        Subcategory::create([
          'category_id' => 3,
          'name' => 'Subcategory 9'
        ]);
    }
}
