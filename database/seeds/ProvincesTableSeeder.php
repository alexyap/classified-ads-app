<?php

use Illuminate\Database\Seeder;
use App\Province;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::create([
          'name' => 'Province 1'
        ]);

        Province::create([
          'name' => 'Province 2'
        ]);

        Province::create([
          'name' => 'Province 3'
        ]);
    }
}
