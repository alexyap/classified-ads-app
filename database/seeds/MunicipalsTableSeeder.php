<?php

use Illuminate\Database\Seeder;
use App\Municipal;

class MunicipalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Municipal::create([
          'province_id' => 1,
          'name' => 'Municipal 1',
        ]);

        Municipal::create([
          'province_id' => 1,
          'name' => 'Municipal 2',
        ]);

        Municipal::create([
          'province_id' => 1,
          'name' => 'Municipal 3',
        ]);

        Municipal::create([
          'province_id' => 2,
          'name' => 'Municipal 4',
        ]);

        Municipal::create([
          'province_id' => 2,
          'name' => 'Municipal 5',
        ]);

        Municipal::create([
          'province_id' => 2,
          'name' => 'Municipal 6',
        ]);

        Municipal::create([
          'province_id' => 3,
          'name' => 'Municipal 7',
        ]);

        Municipal::create([
          'province_id' => 3,
          'name' => 'Municipal 8',
        ]);

        Municipal::create([
          'province_id' => 3,
          'name' => 'Municipal 9',
        ]);
    }
}
