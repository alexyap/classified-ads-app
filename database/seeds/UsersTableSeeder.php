<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'name' => 'Test 1',
          'email' => 'test1@test.com',
          'password' => Hash::make('secret'),
        ]);

        User::create([
          'name' => 'Test 2',
          'email' => 'test2@test.com',
          'password' => Hash::make('secret'),
        ]);

        User::create([
          'name' => 'Test 3',
          'email' => 'test3@test.com',
          'password' => Hash::make('secret'),
        ]);
    }
}
