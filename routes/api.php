<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
  $this->post('/login', 'Api\AuthController@login');
  $this->post('/logout', 'Api\AuthController@logout');

  $this->get('/categories', 'Api\CategoryController@index');
  $this->get('/category', 'Api\CategoryController@show');
  $this->get('/ads', 'Api\AdController@index');
  $this->get('/ad/{slug}', 'Api\AdController@show');
  $this->get('/comments', 'Api\CommentController@index');
  $this->get('/comment/{id}', 'Api\CommentController@show');
  $this->get('/replies', 'Api\ReplyController@index');
  $this->get('/reply/{id}', 'Api\ReplyController@show');
  $this->get('/provinces', 'Api\ProvinceController@index');
  $this->get('/province/{id}', 'Api\ProvinceController@show');
  $this->get('/municipals', 'Api\MunicipalController@index');
  $this->get('/municipal/{id}', 'Api\MunicipalController@show');

  Route::group(['prefix' => 'auth'], function ($router) {
      Route::post('login', 'AuthController@login');
      Route::post('logout', 'AuthController@logout');
      Route::post('refresh', 'AuthController@refresh');
      Route::post('me', 'AuthController@me');
  });
});
