import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    categories: '',
    ads: {
      data: '',
      links: '',
      meta: ''
    },
  },

  getters: {

  },

  mutations: {
    SET_CATEGORIES: (state, payload) => {
      state.categories = payload;
    },

    SET_ADS: (state, payload) => {
      state.ads.data = payload.data;
      state.ads.links = payload.links;
      state.ads.meta = payload.meta;
    }
  },

  actions: {
    getCategories: context => {
      axios.get('/api/v1/categories')
        .then(response => {
          const { data } = response.data;
          context.commit('SET_CATEGORIES', data);
        })
        .catch(e => {
          console.error(e);
        });
    },

    getAllAds: (context, payload) => {
      if (payload.subcategory) {
        axios.get(`/api/v1/ads?subcategory=${payload.subcategory}`)
          .then(response => {
            const { data, links, meta } = response.data;
            const payload = {
              data: data,
              links: links,
              meta: meta
            };
            console.log(response)
            context.commit('SET_ADS', payload);
          })
          .catch(e => {
            console.error(e);
          });
      } else if (payload.queryString) {
        axios.get(`/api/v1/ads?category=${payload.category}&${payload.queryString}`)
          .then(response => {
            const { data, links, meta } = response.data;
            const payload = {
              data: data,
              links: links,
              meta: meta
            };
            context.commit('SET_ADS', payload);
          })
          .catch(e => {
            console.error(e);
          });
      } else if (payload.category === 'all-results') {
        axios.get(`/api/v1/ads?${payload.queryString}`)
          .then(response => {
            const { data, links, meta } = response.data;
            const payload = {
              data: data,
              links: links,
              meta: meta
            };
            context.commit('SET_ADS', payload);
          })
          .catch(e => {
            console.error(e);
          });
      } else {
        axios.get(`/api/v1/ads?category=${payload.category}`)
          .then(response => {
            const { data, links, meta } = response.data;
            const payload = {
              data: data,
              links: links,
              meta: meta
            };
            context.commit('SET_ADS', payload);
          })
          .catch(e => {
            console.error(e);
          });
      }
    },

    loginUser: (context, payload) => {
      axios.post('/api/v1/auth/login', payload)
        .then(response => {
          const { data } = response.data;
          console.log(data);
        })
        .catch(e => {
          console.error(e);
        });
    }
  }
});
