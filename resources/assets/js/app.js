require('./bootstrap');

import Vue from 'vue';
import MainComponent from './components/MainComponent.vue';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueRouter from 'vue-router';
import { routes } from './routes/routes';
import { store } from './store/store';

Vue.use(BootstrapVue);
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes
});

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
      MainComponent
    }
});
