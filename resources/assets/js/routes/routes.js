import LandingPage from '../pages/LandingPage.vue';
import AdList from '../pages/AdList.vue';

export const routes = [
  {
    path: '/',
    component: LandingPage
  },
  {
    path: '/:category',
    component: AdList
  },
  {
    path: '/:category/:subcategory',
    component: AdList
  }
]
