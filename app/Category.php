<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    protected $fillable = [
      'name'
    ];

    public function subcategories()
    {
      return $this->hasMany(Subcategory::class);
    }

    public function ads()
    {
      return $this->hasManyThrough(Ad::class, Subcategory::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
