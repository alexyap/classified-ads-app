<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = [
      'user_id', 'comment_id', 'content'
    ];

    public function user()
    {
      return $this->hasMany(Reply::class);
    }

    public function comment()
    {
      return $this->belongsTo(Comment::class);
    }
}
