<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
      'user_id', 'ad_id', 'file_name'
    ];
}
