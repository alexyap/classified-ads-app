<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Ad;
use App\Category;
use App\Subcategory;
use App\Http\Resources\Ad as AdResource;

class AdController extends Controller
{
    public function index(Request $request)
    {
      $category;
      $ads;

      if ($request->query('category') && $request->query('category') !== 'all-results') {
        $category = Category::where('slug', $request->query('category'))->first();
        $ads = $category->ads();
      } else if ($request->query('q') && $request->query('category') === 'all-results') {
        $query = "%" . $request->query('q') . "%";
        $ads = Ad::where('title', 'like', $query)
                ->orWhere('description', 'like', $query);

        // $paginator = \DB::table('ads')
        //           ->leftJoin('users', 'users.id', '=', 'ads.user_id')
        //           ->leftJoin('municipals', 'municipals.id', '=', 'users.municipal_id')
        //           ->leftJoin('provinces', 'provinces.id', '=', 'municipals.province_id')
        //           ->where('ads.title', 'like', $query)
        //           ->orWhere('ads.description', 'like', $query)
        //           ->orWhere('users.name', 'like', $query)
        //           ->orWhere('municipals.name', 'like', $query)
        //           ->orWhere('provinces.name', 'like', $query)
        //           ->paginate(15);
        // $ads = Ad::hydrate($paginator->items());
        // dd($ads);
        // return AdResource::collection($ads);
      }

      if (!is_null($request->query('condition'))) {
        $ads = $ads->where('is_new', $request->query('condition'));
      }

      if ($request->query('minPrice') && $request->query('maxPrice')) {
        $minPrice = $request->query('minPrice') * 100;
        $maxPrice = $request->query('maxPrice') * 100;
        $ads = $ads->whereBetween('price', [$minPrice, $maxPrice]);
      }

      if ($request->query('sortBy') && $request->query('orderBy')) {
        $ads = $ads->orderBy($request->query('sortBy'), $request->query('orderBy'));
      }

      return AdResource::collection($ads->paginate(15));
    }
}
