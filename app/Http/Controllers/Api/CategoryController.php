<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Resources\Category as CategoryResource;

class CategoryController extends Controller
{
    public function index()
    {
      $categories = Category::all();
      return CategoryResource::collection($categories);
    }

    public function show()
    {

    }
}
