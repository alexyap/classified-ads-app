<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class Ad extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'owner' => new UserResource($this->owner),
          'subcategory' => $this->subcategory,
          'title' => $this->title,
          'description' => $this->description,
          'price' => $this->price / 100,
          'slug' => $this->slug,
          'condition' => $this->condition,
          'created' => $this->created_at->diffForHumans(),
          'updated' => $this->updated_at->diffForHumans(),
        ];
    }
}
