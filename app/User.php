<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'mobile_number', 'password', 'municipal_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['location'];

    public function municipal()
    {
      return $this->belongsTo(Municipal::class);
    }

    public function ads()
    {
      return $this->hasMany(Ad::class);
    }

    public function comments()
    {
      return $this->hasMany(Comment::class);
    }

    public function replies()
    {
      return $this->hasMany(Reply::class);
    }

    public function getLocationAttribute()
    {
      if ($this->municipal) {
        return $this->municipal->name . ', ' . $this->municipal->province->name;
      }

      return null;
    }
}
