<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Municipal extends Model
{
    use Sluggable;
    
    protected $fillable = [
      'province_id', 'name', 'slug'
    ];

    public function province()
    {
      return $this->belongsTo(Province::class);
    }

    public function users()
    {
      return $this->hasMany(User::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
