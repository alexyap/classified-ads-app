<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Province extends Model
{
    use Sluggable;
    
    protected $fillable = [
      'name', 'slug'
    ];

    public function municipals()
    {
      return $this->hasMany(Municipal::class);
    }

    public function users()
    {
      return $this->hasManyThrough(Municipal::class, User::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
