<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Laravel\Scout\Searchable;

class Ad extends Model
{
    use Sluggable, Searchable;

    protected $fillable = [
      'user_id', 'subcategory_id', 'title', 'description', 'price', 'slug', 'is_new'
    ];

    protected $appends = ['condition'];

    public function owner()
    {
      return $this->belongsTo(User::class, 'user_id');
    }

    public function subcategory()
    {
      return $this->belongsTo(Subcategory::class);
    }

    public function comments()
    {
      return $this->hasMany(Comment::class);
    }

    public function getRouteKeyName()
    {
      return 'slug';
    }

    public function getConditionAttribute()
    {
      if ($this->is_new) {
        return 'Brand New';
      }

      return 'Used';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
