<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
      'user_id', 'ad_id', 'content'
    ];

    public function ad()
    {
      return $this->belongsTo(Ad::class);
    }

    public function replies()
    {
      return $this->hasMany(Reply::class);
    }
}
