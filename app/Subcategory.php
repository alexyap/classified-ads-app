<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Subcategory extends Model
{
    use Sluggable;

    protected $fillable = [
      'category_id', 'name'
    ];

    public function category()
    {
      return $this->belongsTo(Category::class);
    }

    public function ads()
    {
      return $this->hasMany(Ad::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
